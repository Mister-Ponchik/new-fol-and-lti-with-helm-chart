package hsostarter.jvm.http

import cats.effect.{Blocker, ContextShift, IO}
import cats.syntax.either._
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64
import hsostarter.jvm.config.AppConfig
import io.circe._
import io.circe.generic.auto._
import io.circe.literal.JsonStringContext
import io.circe.parser._
import io.circe.syntax._
import org.http4s.Method._
import org.http4s._
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.circe._
import org.http4s.client.JavaNetClientBuilder
import org.http4s.client.dsl.io._
import org.http4s.dsl.Http4sDsl
import org.http4s.headers._
import org.http4s.implicits._
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}

import java.text.SimpleDateFormat
import java.time.Instant
import java.util.concurrent._
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Random, Success}

object lti extends Http4sDsl[IO] {

  // def id: Either[String,Int]

  case class Lti13Login(
    iss: String,
    target_link_uri: Option[String],
    login_hint: Option[String],
    lti_message_hint: Option[String],
    client_id: Option[String],
    lti_deployment_id: Option[String],
  )
  case class login13(
    iss: String,
    targetLinkUri: Option[String],
    loginHint: Option[String],
    ltiMessageHint: Option[String],
    clientId: Option[String],
    ltiDeploymentId: Option[String],
  )
  implicit val ioContextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  // implicit val decoder: EntityDecoder[IO, Json] = jsonOf[IO, Json]
  final protected val nonceBuffer: Array[Byte] = new Array[Byte](16)

  def generateNonce = synchronized {
    scala.util.Random.nextBytes(nonceBuffer)
    // let's use base64 encoding over hex, slightly more compact than hex or decimals
    Base64.encode(nonceBuffer)
  }
  val safeChars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwqyx0123456789"
  def safeString(n: Int): String =
    (0 until n).map { _ =>
      safeChars(Random.nextInt(safeChars.length))
    }.mkString

  def jwtmake = {
    val iat = Instant.now.getEpochSecond
    val exp = Instant.now.getEpochSecond + 3600
    val calim = JwtClaim(
      s"""{"iss": "${AppConfig.baseUrl}",
          "iat": $iat,
          "exp": $exp,
          "aud": "${AppConfig.baseUrl}/mod/lti/token.php",
          "sub": "VYcwJrz8G1pvncu"}""",
    )
    val key =
      "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC7VJTUt9Us8cKj\nMzEfYyjiWA4R4/M2bS1GB4t7NXp98C3SC6dVMvDuictGeurT8jNbvJZHtCSuYEvu\nNMoSfm76oqFvAp8Gy0iz5sxjZmSnXyCdPEovGhLa0VzMaQ8s+CLOyS56YyCFGeJZ\nqgtzJ6GR3eqoYSW9b9UMvkBpZODSctWSNGj3P7jRFDO5VoTwCQAWbFnOjDfH5Ulg\np2PKSQnSJP3AJLQNFNe7br1XbrhV//eO+t51mIpGSDCUv3E0DDFcWDTH9cXDTTlR\nZVEiR2BwpZOOkE/Z0/BVnhZYL71oZV34bKfWjQIt6V/isSMahdsAASACp4ZTGtwi\nVuNd9tybAgMBAAECggEBAKTmjaS6tkK8BlPXClTQ2vpz/N6uxDeS35mXpqasqskV\nlaAidgg/sWqpjXDbXr93otIMLlWsM+X0CqMDgSXKejLS2jx4GDjI1ZTXg++0AMJ8\nsJ74pWzVDOfmCEQ/7wXs3+cbnXhKriO8Z036q92Qc1+N87SI38nkGa0ABH9CN83H\nmQqt4fB7UdHzuIRe/me2PGhIq5ZBzj6h3BpoPGzEP+x3l9YmK8t/1cN0pqI+dQwY\ndgfGjackLu/2qH80MCF7IyQaseZUOJyKrCLtSD/Iixv/hzDEUPfOCjFDgTpzf3cw\nta8+oE4wHCo1iI1/4TlPkwmXx4qSXtmw4aQPz7IDQvECgYEA8KNThCO2gsC2I9PQ\nDM/8Cw0O983WCDY+oi+7JPiNAJwv5DYBqEZB1QYdj06YD16XlC/HAZMsMku1na2T\nN0driwenQQWzoev3g2S7gRDoS/FCJSI3jJ+kjgtaA7Qmzlgk1TxODN+G1H91HW7t\n0l7VnL27IWyYo2qRRK3jzxqUiPUCgYEAx0oQs2reBQGMVZnApD1jeq7n4MvNLcPv\nt8b/eU9iUv6Y4Mj0Suo/AU8lYZXm8ubbqAlwz2VSVunD2tOplHyMUrtCtObAfVDU\nAhCndKaA9gApgfb3xw1IKbuQ1u4IF1FJl3VtumfQn//LiH1B3rXhcdyo3/vIttEk\n48RakUKClU8CgYEAzV7W3COOlDDcQd935DdtKBFRAPRPAlspQUnzMi5eSHMD/ISL\nDY5IiQHbIH83D4bvXq0X7qQoSBSNP7Dvv3HYuqMhf0DaegrlBuJllFVVq9qPVRnK\nxt1Il2HgxOBvbhOT+9in1BzA+YJ99UzC85O0Qz06A+CmtHEy4aZ2kj5hHjECgYEA\nmNS4+A8Fkss8Js1RieK2LniBxMgmYml3pfVLKGnzmng7H2+cwPLhPIzIuwytXywh\n2bzbsYEfYx3EoEVgMEpPhoarQnYPukrJO4gwE2o5Te6T5mJSZGlQJQj9q4ZB2Dfz\net6INsK0oG8XVGXSpQvQh3RUYekCZQkBBFcpqWpbIEsCgYAnM3DQf3FJoSnXaMhr\nVBIovic5l0xFkEHskAjFTevO86Fsz1C2aSeRKSqGFoOQ0tmJzBEs1R6KqnHInicD\nTQrKhArgLXX4v3CddjfTRJkFWDbE/CkvKZNOrcf1nhaGCPspRJj2KUkj1Fhl9Cnc\ndn/RsYEONbwQSjIfMPkvxF+8HQ==\n-----END PRIVATE KEY-----"

    val algo = JwtAlgorithm.RS256
    val token = JwtCirce.encode(calim, key, algo)
    token
  }

  def accessreq = {
    val blockingPool = Executors.newFixedThreadPool(5)
    val blocker = Blocker.liftExecutorService(blockingPool)
    val httpClient = JavaNetClientBuilder[IO](blocker).create
    val req =
      POST(
        UrlForm(
          "grant_type" -> "client_credentials",
          "client_assertion_type" -> "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
          "client_assertion" -> jwtmake,
          "scope" -> "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score",
        ),
        uri"${AppConfig.baseUrl}/mod/lti/token.php",
      )
    val accessreq = httpClient.expect[String](req)
    accessreq
      .unsafeRunSync()

  }

  def sendscore(score: Int, user_id: Int, lineitem: String) = {
    val blockingPool = Executors.newFixedThreadPool(5)
    val blocker = Blocker.liftExecutorService(blockingPool)
    val httpClient = JavaNetClientBuilder[IO](blocker).create

    val data =
      s"""
         |{
         |  "activityProgress": "Completed",
         |  "comment": "string",
         |  "gradingProgress": "FullyGraded",
         |  "scoreGiven": $score,
         |  "scoreMaximum": 100,
         |  "timestamp": "2022-01-31T23:24:33.336Z",
         |  "userId": $user_id
         |}
         |
         |""".stripMargin

    val dataparsed = parse(data)
    // accessreq.slice(22, 55)
    val authorizationHeader = Headers.of(
      Authorization(
        Credentials.Token(AuthScheme.Bearer, accessreq.slice(22, 54)),
      ),
    )
    def convert(): String = {
      val ts = System.currentTimeMillis()
      val df: SimpleDateFormat = new SimpleDateFormat(
        "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
      )
      df.format(ts)
    }
    val body =
      json"""{"activityProgress": "Completed","comment": "Nice!","gradingProgress": "FullyGraded","scoreGiven": $score,"scoreMaximum": 50,"timestamp": $convert,"userId": $user_id}"""
    val mymedia =
      new MediaType("application", "vnd.ims.lis.v1.score+json")
    // http://localhost:8090/mod/lti/services.php/2/lineitems/2/lineitem/scores?type_id=1
    val myrequest = Request[IO](
      Method.POST,
      Uri
        .fromString(lineitem).toOption.get,
      HttpVersion.`HTTP/1.1`,
      headers = authorizationHeader,
    ).withEntity(body).withContentType(
        `Content-Type`(mymedia),
      )

    val postreq = httpClient.expect[String](
      myrequest,
    )
    postreq.unsafeRunSync()

  }

  def sendgettolms(
    clientid: String,
    targetlink: String,
    randstate: String,
    randnonce: String,
    loginhint: String,
    lmh: String,
  ) = {

    val blockingPool = Executors.newFixedThreadPool(5)
    val blocker = Blocker.liftExecutorService(blockingPool)
    val httpClient = JavaNetClientBuilder[IO](blocker).create
    // val link =
    //  s"http://localhost:8090/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=$clientid&redirect_uri=$targetlink&state=$randstate&nonce=$randnonce&login_hint=$loginhint&lti_message_hint=$lmh"

    val request = POST(
      UrlForm(
        ("scope", "openid"),
        ("response_type", "id_token"),
        ("response_mode", "form_post"),
        ("prompt", "none"),
        ("client_id", "7bw779F24uuwy0X"),
        ("redirect_uri", "http://127.0.0.1:8090/login"),
        ("state", "NoCqoVOk7RR1HLQEWo7vRlpq"),
        ("login_hint", "2"),
        ("lti_message_hint", "15"),
      ),
      uri"${AppConfig.baseUrl}/mod/lti/auth.php?",
      Accept(MediaType.application.json),
    )

    val req = GET(
      // uri"http://localhost:8090/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=$clientid&redirect_uri=$targetlink&state=$randstate&nonce=$randnonce&login_hint=$loginhint&lti_message_hint=$lmh",
      // uri"http://localhost:8090/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=7bw779F24uuwy0X&redirect_uri=http%3A%2F%2F127.0.0.1%3A8090%2F&state=NoCqoVOk7RR1HLQEWo7vRlpq&nonce=fvwapGVEa4gBNpEU6mep4GaY&login_hint=2&lti_message_hint=15",
      uri"${AppConfig.baseUrl}/mod/lti/auth.php?"
        .withQueryParam("scope", "openid").withQueryParam(
          "response_type",
          "id_token",
        ).withQueryParam("response_mode", "form_post").withQueryParam(
          "prompt",
          "none",
        ).withQueryParam("client_id", clientid).withQueryParam(
          "redirect_uri",
          "http://127.0.0.1:8090/",
        ).withQueryParam("state", randstate).withQueryParam(
          "nonce",
          randnonce,
        ).withQueryParam("login_hint", loginhint).withQueryParam(
          "lti_message_hint",
          lmh,
        ),
    )
    val postval =
      """
        |{
        |"scope":"openid",
        |"response_type":"id_token",
        |"response_mode":"form_post",
        |"prompt":"none",
        |"client_id":"7bw779F24uuwy0X",
        |"redirect_uri":"http%3A%2F%2F127.0.0.1%3A8090%2F",
        |"state":"NoCqoVOk7RR1HLQEWo7vRlpq",
        |"nonce":"fvwapGVEa4gBNpEU6mep4GaY",
        |"login_hint": 2,
        |"lti_message_hint": 15
        |
        |}
        |"""

    val jsonpostval = parse(postval)

    val getreq = httpClient.expect[String](
      req,
      // request,
      // s"http://localhost:8090/mod/lti/ausendgettolmth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=$clientid&redirect_uri=$targetlink&state=$randstate&nonce=$randnonce&login_hint=$loginhint&lti_message_hint=$lmh",
      // uri"localhost:8090/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=7bw779F24uuwy0X&redirect_uri=http://127.0.0.1:8090/&state=NoCqoVOk7RR1HLQEWo7vRlpq&nonce=fvwapGVEa4gBNpEU6mep4GaY&login_hint=2&lti_message_hint=15",
      // uri"http://localhost:8090/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=7bw779F24uuwy0X&redirect_uri=http://127.0.0.1:8090/login&state=NoCqoVOk7RR1HLQEWo7vRlpq&nonce=fvwapGVEa4gBNpEU6mep4GaY&login_hint=2&lti_message_hint=15",
      // "http://127.0.0.1:8090/ispnf?answer=asda&randfor=asda",
    )
    getreq.unsafeRunSync()

    // req

  }
  var id: Either[String, Int] = Right(-1)
  var jwt: String = ""
  // var token: String = ""
  case class Token(token: String)
  // implicit val decoder = jsonOf[IO, Token]
  def endpoints(): HttpRoutes[IO] =
    HttpRoutes.of[IO] {
      case request @ POST -> Root / "login" =>
        // for {
        // lti13 <- request.as[String]

        // resp <- Ok(
        // login13(
        // parse(lti13).,
        // lti13.target_link_uri,
        // lti13.login_hint,
        // lti13.lti_message_hint,
        // lti13.client_id,
        // lti13.lti_deployment_id,
        // ).asJson,
        // )
        // for {
        // iss <- request.params.get("iss")
        // targeturl <- request.params.get("target_link_uri")
        // loginhint <- request.params.get("login_hint")
        // ltimeshint <- request.params.get("lti_message_hint")
        // clientid <- request.params.get("client_id")
        // ltiDeploymentId <- request.params.get("lti_deployment_id")

        // } yield {

        //  login13(iss,Some(targeturl),Some(loginhint),Some(ltimeshint),Some(clientid),Some(ltiDeploymentId))
        // }
        // val resource = for {
        // client <- BlazeClientBuilder[IO](scala.concurrent.ExecutionContext.Implicits.global).resource
        // } yield client
        // resource.use(client => client.expect[String](Uri.unsafefromString("http://localhost:8090"))).unsafeRunSync()
        // val response = client.expect[String](Uri.unsafefromString("http://localhost:8888/hello"))

        val str = request.as[String].unsafeRunSync()
        val clientid = str.substring(126, 141)
        val loginhint = str.substring(94, 95)
        val lmh = str.substring(113, 115)
        val targetlink = str.substring(50, 82)
        // val targetlink = "http://127.0.0.1:8090/login"
        val randstate = safeString(28)
        val randnonce = safeString(30)
        val link =
          AppConfig.baseUrl + s"/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=$clientid&redirect_uri=$targetlink&state=$randstate&nonce=$randnonce&login_hint=$loginhint&lti_message_hint=$lmh"
        // val validUri = Uri.fromString(link).toOption.get

        // val requestget: Request[IO] = Request[IO](Method.GET, validUri)
        // httpClient.run(requestget)
        // Ok(accessreq)

        Ok(
          sendgettolms(
            clientid,
            targetlink,
            randstate,
            randnonce,
            loginhint,
            lmh,
          ),
          `Content-Type`(MediaType.text.html),
          // sendgettolms(),
        )
        PermanentRedirect(
          Location(
            uri"${AppConfig.baseUrl}/mod/lti/auth.php?"
              .withQueryParam("scope", "openid").withQueryParam(
                "response_type",
                "id_token",
              ).withQueryParam("response_mode", "form_post").withQueryParam(
                "prompt",
                "none",
              ).withQueryParam("client_id", clientid).withQueryParam(
                "redirect_uri",
                "http://127.0.0.1:8090/getjwt",
              ).withQueryParam("state", randstate).withQueryParam(
                "nonce",
                randnonce,
              ).withQueryParam("login_hint", loginhint).withQueryParam(
                "lti_message_hint",
                lmh,
              ),
          ),
        )
      // TemporaryRedirect(Uri(path = "/"))
      // } yield Ok(lti13)

      case request @ POST -> Root / "scores" =>
        val public_key =
          "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu1SU1LfVLPHCozMxH2Mo\n4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0/IzW7yWR7QkrmBL7jTKEn5u\n+qKhbwKfBstIs+bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyeh\nkd3qqGElvW/VDL5AaWTg0nLVkjRo9z+40RQzuVaE8AkAFmxZzow3x+VJYKdjykkJ\n0iT9wCS0DRTXu269V264Vf/3jvredZiKRkgwlL9xNAwxXFg0x/XFw005UWVRIkdg\ncKWTjpBP2dPwVZ4WWC+9aGVd+Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbc\nmwIDAQAB\n-----END PUBLIC KEY-----"
        val req = JwtCirce.decodeJson(
          request.as[String].unsafeRunSync(),
          public_key,
          Seq(JwtAlgorithm.RS256),
        )
        req match {
          case Success(jwtJson) =>
            val user_id = jwtJson.hcursor.get[Int]("id").getOrElse(-3)
            val score = jwtJson.hcursor.get[Int]("score").getOrElse(0)
            val lineitem = jwtJson.hcursor
              .get[String]("lineitem").getOrElse(
                AppConfig.baseUrl + "/mod/lti/services.php/2/lineitems/7/lineitem/scores?type_id=1",
              )
            Ok(sendscore(score, user_id, lineitem))
          case Failure(exception) => Ok()
        }

      case request @ POST -> Root / "getjwt" =>
        val public_key =
          "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt6W5KyVO8eVHs5wtGnV+\nvfibiMQ0uwTqoaoWhV120WElEmFjwacZ6YLyxD0yxo6Ae5EufUR/SXTv9TCg9pHU\nuF8pkv5vVt+HnhnS5PPHkJaCxYDnROk2k17lAW9i4lbMD+DEtuMxnRXEvFjFVT3a\n5JoYB/mePG+ktGE3YnoCYbqMiXTDQ31Q+K0AmQTnSsZP1j54IcvqdKEXXmiU/fU9\nwZzBob6p3OfDs+4HcS7NnONipRUnnbgxqzw6uOIXeFF2HvoqxSvMski7QkBv/gY/\n4bNZ09+C9X3JvLZnpCTLiI1FrV/FkEWz+XpQGRvl8kICN6qh559GcoooDm5TZHvk\n1QIDAQAB\n-----END PUBLIC KEY-----"
        val token = request.as[UrlForm]
        val tokens = token.map(form => form.getFirst("id_token"))
//        val token1 = for {
//          tok <- tokens
//        } yield tok.getOrElse("defult")
        val tmpjwt = for {
          tok <- tokens
        } yield tok.getOrElse("defult")
        jwt = tmpjwt.unsafeRunSync()

//        JwtCirce
//          .decodeJson(
//            token1.unsafeRunSync(),
//            public_key,
//            Seq(JwtAlgorithm.RS256),
//          ).fold(
//            _.getMessage.asLeft[String],
//            json => {
//              val cursor = json.hcursor
//              id = for {
//                lisString <- cursor
//                  .downField(
//                    "https://purl.imsglobal.org/spec/lti-bo/claim/basicoutcome",
//                  ).get[String]("lis_result_sourcedid").left.map(_.message)
//                rawLis = lisString.replace("""\""", "")
//                dataParsed <- parse(rawLis).left.map(_.message)
//                userId <- dataParsed.hcursor
//                  .downField("data")
//                  .get[Int]("userid").left.map(_.message)
//              } yield userId
//
//            },
//          )

        PermanentRedirect(Location(uri"http://127.0.0.1:8090/"))

      case request @ POST -> Root / "getId" =>
        // val public_key =
        // "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt6W5KyVO8eVHs5wtGnV+\nvfibiMQ0uwTqoaoWhV120WElEmFjwacZ6YLyxD0yxo6Ae5EufUR/SXTv9TCg9pHU\nuF8pkv5vVt+HnhnS5PPHkJaCxYDnROk2k17lAW9i4lbMD+DEtuMxnRXEvFjFVT3a\n5JoYB/mePG+ktGE3YnoCYbqMiXTDQ31Q+K0AmQTnSsZP1j54IcvqdKEXXmiU/fU9\nwZzBob6p3OfDs+4HcS7NnONipRUnnbgxqzw6uOIXeFF2HvoqxSvMski7QkBv/gY/\n4bNZ09+C9X3JvLZnpCTLiI1FrV/FkEWz+XpQGRvl8kICN6qh559GcoooDm5TZHvk\n1QIDAQAB\n-----END PUBLIC KEY-----"
        Ok(
          // EitherId(id),
          jwt,
        )

    }

}
