package hsostarter.jvm.http

import cats.effect.{ContextShift, IO}
import fastparse.Parsed
import hsostarter.models.isnorm
import io.circe.Json
import io.circe.literal.JsonStringContext
import io.circe.syntax._
import org.http4s.blaze.http.http2.PseudoHeaders.Method
import org.http4s.{HttpRoutes, Request, Uri}
import org.http4s.circe._
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.dsl.Http4sDsl
import org.http4s.client.dsl.io._

import scala.concurrent.ExecutionContext

object isNormal extends Http4sDsl[IO] {

  implicit val ioContextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  object ValueMatcher extends QueryParamDecoderMatcher[String]("answer")
  object ranfor extends QueryParamDecoderMatcher[String]("randfor")

  def endpoints(): HttpRoutes[IO] =
    HttpRoutes.of[IO] {
      case GET -> Root / "ispnf" :? ValueMatcher(answer) +& ranfor(randfor) =>
        val p = FormulaParser.Parse(randfor)
        p match {
          case Parsed.Success(value, _) =>
            val pnf = IndexedFormula
              .toPrenex(Formula.toIndexed(value)._1)
            val cond = pnf.toString == answer
            if (cond) {
              // POST(json"""{"name": "Alice"}""","/hello")

              // val data: Json =
              // json"""{"scoreGiven" : "1", "client_id" : "RvotoRGJMqh3Lnx"}"""
              // val uri =
              //  Uri.fromString("http://localhost:8090/scores/lti1p3").toOption.get
              // val request: Request[IO] = Request[IO](method = POST, uri = uri)
              //   .withEntity(data)
              // val io = endpoints().run(request)

              Ok(isnorm("Right!").asJson)

            } else Ok(isnorm("Wrong!").asJson)

          case failure: Parsed.Failure =>
            Ok(
              isnorm(
                "\"Incorrect formula, error:\\n\" + failure.trace()",
              ).asJson,
            )
        }

      // val pnf = IndexedFormula
      // .toPrenex(Formula.toIndexed(p )._1)

    }
}
