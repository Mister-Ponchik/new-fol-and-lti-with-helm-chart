package hsostarter.jvm.http

import cats.effect.{ContextShift, IO}
import fastparse.Parsed
import hsostarter.jvm.http.IndexedFormula.toPrenex
import hsostarter.models.isnorm
import io.circe.syntax._
import org.http4s.HttpRoutes
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

import scala.concurrent.ExecutionContext

object compare extends Http4sDsl[IO] {

  implicit val ioContextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  object ValueMatcher extends QueryParamDecoderMatcher[String]("for1")
  object ranfor extends QueryParamDecoderMatcher[String]("for2")

  def endpoints(): HttpRoutes[IO] =
    HttpRoutes.of[IO] {
      case GET -> Root / "compare" :? ValueMatcher(for1) +& ranfor(for2) =>
        val l = FormulaParser.Parse(for1)
        val r = FormulaParser.Parse(for2)

        l match {
          case Parsed.Success(v1, _) =>
            r match {
              case Parsed.Success(v2, _) =>
                if (v1 == v2)
                  Ok(isnorm(for1 + " and " + for2 + " are same!").asJson)
                else {
                  val p1 = toPrenex(Formula.toIndexed(v1)._1)
                  val p2 = toPrenex(Formula.toIndexed(v2)._1)

                  if (p1 == p2) Ok(isnorm("Right!").asJson)
                  else Ok(isnorm("Wrong!").asJson)

                }

              case failure: Parsed.Failure =>
                Ok(
                  isnorm("Incorrect formula, error:\n" + failure.trace()).asJson,
                )

            }
          case failure: Parsed.Failure =>
            Ok(isnorm("Incorrect formula, error:\n" + failure.trace()).asJson)

        }

      // val pnf = IndexedFormula
      // .toPrenex(Formula.toIndexed(p )._1)

    }
}
