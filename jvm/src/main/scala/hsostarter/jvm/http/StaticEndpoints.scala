package hsostarter.jvm.http

import cats.effect.{Blocker, ContextShift, Effect}
import org.http4s._
import org.http4s.dsl.Http4sDsl

final class StaticEndpoints[F[_]: ContextShift: Effect](
  assetsPath: String,
  blocker: Blocker,
) extends Http4sDsl[F] {

  private[this] def static(
    file: String,
    request: Request[F],
  ) =
    StaticFile
      .fromResource("/static/" + file, blocker, Some(request))
      .getOrElseF(NotFound())

  def endpoints(): HttpRoutes[F] =
    HttpRoutes.of[F] {
      case request @ GET -> Root =>
        static("index.html", request)

      case request @ GET -> path
          if List(".js", ".css", ".html", ".ico")
            .exists(path.toString.endsWith) =>
        val fullPath = path.toList.mkString("/")
        static(fullPath, request)
    }

}
