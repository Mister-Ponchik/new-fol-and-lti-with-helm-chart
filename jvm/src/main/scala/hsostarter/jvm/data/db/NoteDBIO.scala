package hsostarter.jvm.data.db

import java.time.Instant

import hsostarter.jvm.data.db.SlickPgProfile.api._
import hsostarter.jvm.models.{Note, NoteData, NoteReference}
import io.circe.{Decoder, Encoder, Json}
import slick.dbio.DBIO
import slick.lifted.{MappedProjection, ProvenShape}

import cats.syntax.either._
import cats.syntax.option._

object NoteDBIO {

  final class DataTable(tag: Tag) extends Table[Note](tag, "note") {

    def id = column[Note.Id]("id", O.AutoInc, O.PrimaryKey)
    def instant = column[Instant]("instant")
    def text = column[String]("text")

    def referenceJson = column[Json]("reference")

    def reference: MappedProjection[Option[NoteReference], Json] =
      referenceJson <> (
        Decoder[Option[NoteReference]].decodeJson(_).valueOr(throw _),
        Encoder[Option[NoteReference]](implicitly)(_).some,
      )

    def data =
      (
        instant,
        text,
        reference,
      ) <>
        ((NoteData.apply _).tupled, NoteData.unapply)

    def * : ProvenShape[Note] =
      (
        id,
        data,
      ) <>
        ((Note.apply _).tupled, Note.unapply)

  }

  object Queries {

    val table = TableQuery(new DataTable(_))
    def schema = table.schema

    val data = Compiled(table.map(_.data))

    val byId = Compiled((id: Rep[Note.Id]) => table.filter(_.id === id))

  }

  def byId(id: Note.Id): DBIO[Option[Note]] =
    Queries.byId(id).result.headOption

  def insert(data: NoteData): DBIO[Note.Id] =
    Queries.data.returning(Queries.table.map(_.id)) += data

  def update(note: Note): DBIO[Int] =
    Queries.table.update(note)

  def delete(id: Note.Id): DBIO[Int] =
    Queries.byId(id).delete

}
