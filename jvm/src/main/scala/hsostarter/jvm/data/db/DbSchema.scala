package hsostarter.jvm.data.db

object DbSchema {
  lazy val createStatementsList =
    NoteDBIO.Queries.schema.createStatements.toList
}
