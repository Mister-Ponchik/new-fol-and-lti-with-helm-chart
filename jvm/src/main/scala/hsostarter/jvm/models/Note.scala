package hsostarter.jvm.models

import java.time.{Instant, Year}

import cats.effect.Sync
import hsostarter.jvm.models
import io.circe.generic.JsonCodec
import org.http4s.circe._
import org.http4s.{EntityDecoder, EntityEncoder}

@JsonCodec sealed trait NoteReference

final case class PlaceReference(
  country: String,
  city: String,
  street: String,
  building: Int,
) extends NoteReference

final case class PersonRefenence(
  name: String,
  yearOfBirth: Year,
) extends NoteReference

object NoteReference

@JsonCodec final case class NoteData(
  instant: Instant,
  text: String,
  reference: Option[NoteReference],
)

object NoteData {

  implicit def noteDataEntityDecoder[F[_]: Sync]: EntityDecoder[F, NoteData] =
    jsonOf[F, NoteData]

}

@JsonCodec final case class Note(
  id: Note.Id,
  data: NoteData,
)

object Note {

  type Id = models.Id[Note, Int]

  implicit def optionNoteEntityEncoder[F[_]]: EntityEncoder[F, Option[Note]] =
    jsonEncoderOf[F, Option[Note]]

  implicit def noteEntityDecoder[F[_]: Sync]: EntityDecoder[F, Note] =
    jsonOf[F, Note]

}
