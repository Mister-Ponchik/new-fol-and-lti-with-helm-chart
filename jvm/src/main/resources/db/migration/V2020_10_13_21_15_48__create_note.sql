CREATE TABLE note (
  id serial NOT NULL PRIMARY KEY,
  instant timestamp NOT NULL,
  text varchar NOT NULL
)