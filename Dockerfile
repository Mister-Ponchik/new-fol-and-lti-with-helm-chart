FROM node:16-alpine as builder

WORKDIR /app

# Install necessary tools
RUN apk update && \
    apk upgrade && \
    apk add git bash

RUN git config --global url.https://github.com/.insteadOf git://github.com/

# Default to UTF-8 file.encoding
ENV LANG C.UTF-8

RUN { \
        echo '#!/bin/sh'; \
        echo 'set -e'; \
        echo; \
        echo 'dirname "$(dirname "$(readlink -f "$(which javac || which java)")")"'; \
    } > /usr/local/bin/docker-java-home \
    && chmod +x /usr/local/bin/docker-java-home

ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ENV PATH $PATH:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin

ENV JAVA_VERSION 8u111
ENV JAVA_ALPINE_VERSION 8.111.14-r0

RUN set -x && apk add openjdk8 && [ "$JAVA_HOME" = "$(docker-java-home)" ]

# Install sbt
ENV SBT_VERSION 1.8.2
RUN \
  echo "$SBT_OPTS" && \
  apk add --no-cache curl && \
  wget -O sbt-$SBT_VERSION.tgz https://github.com/sbt/sbt/releases/download/v$SBT_VERSION/sbt-$SBT_VERSION.tgz && \
  tar -xzf sbt-$SBT_VERSION.tgz && \
  rm sbt-$SBT_VERSION.tgz && \
  mv sbt /usr/local

# Add sbt to PATH
ENV PATH="/usr/local/sbt/bin:$PATH"

# Copy source code into the container
COPY . .

# Compile the project and create the frontend
RUN sbt reload && sbt clean && sbt update && sbt compile && sbt fastOptJS::webpack && sbt hsostarterJVM/assembly

# Stage 2: Run the fat jar
FROM openjdk:8-jre-alpine

WORKDIR /app

# Copy the jar and frontend files from builder stage
COPY --from=builder /app/jvm/target/scala-2.13/hsostarter.jar /app

EXPOSE 8090

CMD ["java", "-jar", "hsostarter.jar"]