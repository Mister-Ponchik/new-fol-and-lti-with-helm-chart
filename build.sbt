val Http4sVersion = "0.21.31"
val http4sVersion = "0.23.0-RC1"

import com.typesafe.sbt.SbtNativePackager.autoImport.NativePackagerHelper.*
import com.typesafe.sbt.packager.docker.Cmd
import sbtcrossproject.CrossPlugin.autoImport.{CrossType, crossProject}
val tylipPublic =
  "tylip-public" at "https://tylip.jfrog.io/artifactory/tylip-public/"

val myAppBaseUrl = sys.env.getOrElse("MYAPP_BASE_URL", "http://math-complex.localhost")

scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.CommonJSModule) }

Compile / scalaJSLinkerConfig ~= { _.withModuleInitializers(
  ModuleInitializer.mainMethod("hsostarter.jvm.config", "setBaseUrl", List(myAppBaseUrl))
)}

Compile / scalaJSLinkerConfig ~= { _.withModuleInitializers(
  ModuleInitializer.mainMethod("hsostarter.js.config", "setBaseUrl", List(myAppBaseUrl))
)}


val hsostarter =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Full).in(file("."))
    .settings(
      organization := "ru.psttf",
      scalaVersion := "2.13.5",
      version := "0.1.0-SNAPSHOT",
      libraryDependencies ++= Seq(
        "org.typelevel" %%% "cats-effect" % "2.1.4",
        "org.typelevel" %%% "mouse" % "1.0.4",
        "io.circe" %%% "circe-generic" % "0.14.1",
        "io.circe" %%% "circe-literal" % "0.14.1",
        "io.circe" %%% "circe-generic-extras" % "0.14.1",
        "io.circe" %%% "circe-parser" % "0.14.1",
        "com.outr" %%% "scribe" % "3.6.4",

      ),
      scalacOptions ++= Seq(
        "-Ymacro-annotations",
        "-Xlint:-byname-implicit,_",
        "-unchecked",
        "-deprecation",
        "-feature",
        "-language:higherKinds",
        "-Xasync",
      ),
      scalafmtOnCompile := !insideCI.value,
    )
    .jvmSettings(Seq(
    ))

val hsostarterJS = hsostarter.js
  .enablePlugins(ScalaJSBundlerPlugin)
  .disablePlugins(RevolverPlugin)
  .settings(
    resolvers ++= Seq(
      //Resolver.sonatypeRepo("releases"),
      "jitpack" at "https://jitpack.io",
      //Resolver.sonatypeRepo("snapshots"),
    ),
    libraryDependencies ++= Seq(
      "io.github.outwatch" %%% "outwatch" % "1.0.0-RC3",
      "org.typelevel" %%% "simulacrum" % "1.0.0",
      //"org.scala-js" %%% "scalajs-dom" % "1.2.0",
      // "org.typelevel" %% "cats-effect" % "3.3.1",
      //"com.github.OutWatch.outwatch" %%% "outwatch" % "584f3f2c32",
      "com.lihaoyi" %%% "fastparse" % "2.3.2",
      "org.scalatest" %%% "scalatest" % "3.2.2" % "test",
      "com.github.scopt" %%% "scopt" % "4.0.1",
      "com.beachape" %%% "enumeratum" % "1.7.0",
      "io.monix" %%% "monix" % "3.4.0",
      "org.scala-js" %%% "scalajs-dom" % "2.0.0",
      //"fr.hmil" %%% "roshttp" % "3.0.0",
      //"org.scala-js" %%% "scalajs-dom" % "0.9.8",
      //"org.endpoints4s" %%% "xhr-client" % "4.0.0",
      //"org.http4s" %%% "http4s-client" % "0.23.7"
      //"io.scalajs.npm" %%% "jwt-simple" % "0.5.0"
      //"be.doeraene" %%% "scalajs-jquery" % "1.0.0"
      //"org.webjars.npm" %%% "jsonwebtoken" % "8.5.1"
      "org.scala-lang.modules" %%% "scala-async" % "1.0.1",

    ),

    npmDependencies in Compile ++= Seq(
      "jquery" -> "3.3",
      "bootstrap" -> "4.3",
      //"jsonwebtoken" -> "8.5.1",
      "jose" -> "4.8.1"
    ),
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), // LibraryOnly() for faster dev builds
    scalaJSUseMainModuleInitializer := true,
    mainClass in Compile := Some("hsostarter.js.UifierIOApp"),
    useYarn := true, // makes scalajs-bundler use yarn instead of npm

  )

val hsostarterJVM = hsostarter.jvm
  .enablePlugins(AssemblyPlugin)
  .settings(
    name := "fol-and-lti-k8s-starter",
    assemblyJarName in assembly := "hsostarter.jar", // set the name of the assembled jar
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(
      includeScala = true
    ),
    resourceGenerators in Compile += Def.task {
      val webpackMappings = webpack.in(Compile, fullOptJS).in(hsostarterJS, Compile).value
      val resourceDir = (resourceManaged in Compile).value

      webpackMappings.map { file =>
        val targetFile = resourceDir / "static" / "scripts" / file.data.getName
        IO.copyFile(file.data, targetFile)
        targetFile
      }
    }.taskValue,
    mainClass in assembly := Some("hsostarter.jvm.HSOStarterJVM"),
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case "NPM_DEPENDENCIES" => MergeStrategy.discard
      case x => (assemblyMergeStrategy in assembly).value(x)
    },
    resolvers += tylipPublic,
    (unmanagedResourceDirectories in Compile) += (resourceDirectory in(hsostarterJS, Compile)).value,
//    bashScriptExtraDefines += """addJava "-Dassets=${app_home}/../assets"""",
    libraryDependencies ++= Seq(
      "com.lihaoyi" %% "fastparse" % "2.3.2",
      "org.scalatest" %% "scalatest" % "3.2.2" % "test",
      "com.github.scopt" %% "scopt" % "4.0.0-RC2",
      "org.postgresql" % "postgresql" % "42.3.1",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
      "com.github.tminglei" %% "slick-pg" % "0.20.2",
      "com.github.tminglei" %% "slick-pg_circe-json" % "0.20.2",
      "org.flywaydb" % "flyway-core" % "8.2.3",
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "ch.qos.logback" % "logback-classic" % "1.2.9",
      "com.github.pureconfig" %% "pureconfig" % "0.14.1",
      "com.github.pureconfig" %% "pureconfig-cats-effect" % "0.14.1",
      "com.tylip" %% "cats-effect-syntax" % "0.1.3",
      "io.circe" %% "circe-parser" % "0.14.1",
      "com.github.jwt-scala" %% "jwt-circe" % "9.0.3",
    ),
    scalacOptions ++= Seq("-Wconf:cat=lint-multiarg-infix:s","-Xasync"),
  ).enablePlugins(JavaAppPackaging)



disablePlugins(RevolverPlugin)

