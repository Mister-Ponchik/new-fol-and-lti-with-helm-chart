package hsostarter.js

import cats.effect.SyncIO
import colibri.{BehaviorSubject, Subject}
import Editable.ops._
import hsostarter.js.Person.getUserId
import outwatch.{HtmlVNode, VNode}
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import scala.concurrent.duration.DurationInt

final class Compare(editor: Editor[TwoFormula]) {

  val nodeforres: SyncIO[VNode] = for {
    goHandler <- Handler.create[Boolean]

  } yield
    div(
      cls := "row d-flex flex-column col-9 ml-2 nizbtn",
      button(
        "Отправить ответ",
        idAttr := "reductionButton",
        `type` := "submit",
        color := "#000000",
        cls := "btn btn-lg btn-secondary col",
        onClick.use(true) --> goHandler,
      ),
      goHandler.map(if (_) {
        div(
          h1("Результат:"),
          div(
            editor.result
              .map(_.map(formulas => formulas.firstFormula)).map(
                _.getOrElse(""),
              ),
          ),
        )

      } else div()),
    )

  val counter: BehaviorSubject[String] = Subject.behavior("")
  val node: HtmlVNode =
    div(
      h2(
        "Введите формулу 2 равносильной формуле 1",
      ),
      h1("Редактор:"),
      editor.present,
      nodeforres,
    )
}

object Compare {
  def init: SyncIO[Compare] =
    for {

      editor <- TwoFormula(
        hsostarter.js.Formula.randomFormula(3, 4, 4).toString,
        Formula.randomFormula(3, 4, 4).toString,
        // user_id,
        // 4,
        getUserId,
      ).editor
    } yield new Compare(editor)
}
