package hsostarter.js

import colibri.Subject
import cats.data.ValidatedNel
import cats.effect.SyncIO
import cats.syntax.apply._
import colibri.Observer.sink
import colibri.{Observable, Sink}
import fastparse.Parsed
import outwatch.{VDomModifier, VNode}
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import hsostarter.js.Editable.ops._
import hsostarter.js.IndexedFormula.toPrenex
import hsostarter.js.Person.httpPostScore
import hsostarter.js.config.Config
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom
import org.scalajs.dom.XMLHttpRequest

import scala.concurrent.Future
import scala.scalajs.js
import scala.util.{Failure, Success}

final case class TwoFormula(
  firstFormula: String,
  secondFormula: String,
  user_id: Int,
)

object TwoFormula {
  def httpGet(theUrl: String): String = {
    val xmlHttp = new XMLHttpRequest()
    xmlHttp.open("GET", theUrl)
    xmlHttp.send(null)
    dom.window.alert("Отправлен запрос!")

    // val json = JSON.parse(xmlHttp.responseText)
    // val json = js.JSON.parse(xmlHttp.responseText)
    // (json \ 1 \ "value").asInstanceOf[String]
    xmlHttp.responseText.substring(10, 16)
    // xmlHttp.responseText
  }
  val twoformulaPresentable: Presentable[TwoFormula] =
    Presentable(twoformula =>
      span(s"${twoformula.firstFormula} ${twoformula.secondFormula}"),
    )

  implicit val twoformulaEditable: Editable[TwoFormula] =
    twoformula =>
      for {
        firstNameEditor <- twoformula.firstFormula.editor
        lastNameEditor  <- twoformula.secondFormula.editor
      } yield
        new Editor[TwoFormula] {

          def result: Observable[ValidatedNel[String, TwoFormula]] =
            Observable.combineLatestMap(
              firstNameEditor.result,
              lastNameEditor.result,
            )(
              (
                firstNameValidated,
                lastNameValidated,
              ) =>
                (firstNameValidated, lastNameValidated).mapN {
                  var res: String = ""
                  (f1, f2) =>
                    val (l, r) = (
                      FormulaParser.Parse(f1),
                      FormulaParser.Parse(f2),
                    )
                    res = httpGet(
                      Config.baseUrl + s"/compare?for1=$f1&for2=$f2",
                    )

                    if (res == "Right!") {
                      httpPostScore(
                        50,
                        twoformula.user_id,
                        Config.baseUrl + "/mod/lti/services.php/2/lineitems/8/lineitem/scores?type_id=1",
                      )

                    }
                    if (res == "Wrong!") {
                      httpPostScore(
                        0,
                        twoformula.user_id,
                        Config.baseUrl + "/mod/lti/services.php/2/lineitems/8/lineitem/scores?type_id=1",
                      )
                    }

                    TwoFormula.apply(res, "", twoformula.user_id)

                },
            )
          override def isnormal2: Observable[ValidatedNel[String, TwoFormula]] =
            ???

//          def isnormal2: Observable[ValidatedNel[String, TwoFormula]] =
//            Observable.combineLatestMap(
//              firstNameEditor.result,
//              lastNameEditor.result,
//
//            )(
//              (
//                firstNameValidated,
//                lastNameValidated,
//              ) =>
//                (firstNameValidated, lastNameValidated,).mapN(TwoFormula.apply),
//            )
          def present: VDomModifier =
            div(
              div("Формула 1: ", twoformula.firstFormula),
              div("Формула 2:", lastNameEditor.present),
            )

          override def ispnf(
            x: String,
          ): Observable[ValidatedNel[String, TwoFormula]] = ???
        }

}
