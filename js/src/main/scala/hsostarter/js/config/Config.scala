package hsostarter.js.config
object Config {
  var baseUrl: String = _

  def setBaseUrl(url: String): Unit = {
    baseUrl = url
  }
}