package hsostarter.js
import cats.data.ValidatedNel
import colibri.Observable
import fastparse.Parsed
import hsostarter.js.Editable.ops._
import hsostarter.js.config.Config
//import hsostarter.js.jose.importSPKI
import io.circe.{Json, ParsingFailure}
import io.circe.parser._
import org.scalajs.dom
import org.scalajs.dom.{console, fetch, window, XMLHttpRequest}
import outwatch.VDomModifier
import outwatch.dsl._

import scala.scalajs.js.JSConverters._
import scala.util.{Failure, Success}
import scala.concurrent.{Await, Future, Promise}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.async.Async.{async, await}
import scala.concurrent.duration.DurationInt
//import io.scalajs.npm.jwtsimple._
import scala.concurrent.duration.Duration
import org.scalajs.dom.SubtleCrypto
import java.time.LocalDateTime
import scala.scalajs.js
import scala.scalajs.js.JSON
import scala.concurrent.CanAwait

final case class Person(firstName: String, id: Int)

object Person {
  def httpGet(theUrl: String): String = {
    val xmlHttp = new XMLHttpRequest()
    xmlHttp.open("GET", theUrl)
    xmlHttp.send(null)
    dom.window.alert("Отправлен запрос!")

    // val json = JSON.parse(xmlHttp.responseText)
    // val json = js.JSON.parse(xmlHttp.responseText)
    // (json \ 1 \ "value").asInstanceOf[String]
    xmlHttp.responseText.substring(10, 16)
    // xmlHttp.responseText
  }

  def httpPostScore(score: Int, id: Int, lineitem: String) = {

    val xmlHttp = new XMLHttpRequest()

    xmlHttp.open("POST", Config.baseUrl + "/scores")

    // xmlHttp.setRequestHeader("Authorization", "Bearer " + token)
    // val time = LocalDateTime.now().toString
    val data =
      s"""
        |{
        |  "score":$score,
        |  "id": $id,
        |  "lineitem":"$lineitem"
        |}
        |
        |""".stripMargin

    xmlHttp.send(data)

  }

  def getUserId(): Int = {

    val xmlHttp = new XMLHttpRequest()

    xmlHttp.open("POST", Config.baseUrl + "/getId", false)
    // xmlHttp.ontimeout
    xmlHttp.send()
    val public_key =
      "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt6W5KyVO8eVHs5wtGnV+vfibiMQ0uwTqoaoWhV120WElEmFjwacZ6YLyxD0yxo6Ae5EufUR/SXTv9TCg9pHUuF8pkv5vVt+HnhnS5PPHkJaCxYDnROk2k17lAW9i4lbMD+DEtuMxnRXEvFjFVT3a5JoYB/mePG+ktGE3YnoCYbqMiXTDQ31Q+K0AmQTnSsZP1j54IcvqdKEXXmiU/fU9wZzBob6p3OfDs+4HcS7NnONipRUnnbgxqzw6uOIXeFF2HvoqxSvMski7QkBv/gY/4bNZ09+C9X3JvLZnpCTLiI1FrV/FkEWz+XpQGRvl8kICN6qh559GcoooDm5TZHvk1QIDAQAB-----END PUBLIC KEY-----"

    // dom.window.alert(xmlHttp.responseText)

    val data = xmlHttp.responseText
    // dom.window.alert(data)
    console.log("jwt:", data.slice(1, data.length - 1))
    // val s = SubtleCrypto.importKey(data)
//      val key = await(Future {
//        importSPKI(public_key, "RS256")
//      })
//    val payload =
//      jose.jwtVerify(
//        data.slice(1, data.length - 1),
//        importSPKI(public_key, "RS256"),
//      )

//      val id = JSON
//        .parse(
//          payload.payload[
//            "https://purl.imsglobal.org/spec/lti-bo/claim/basicoutcome",
//          ]["lis_result_sourcedid"],
//        )["data"]["userid"].asInstanceOf[Int]
//    val id = JSON
//      .parse(
//        payload.payload.`https://purl.imsglobal.org/spec/lti-bo/claim/basicoutcome`.lis_result_sourcedid,
//      ).hcursor.downField("data").downField("userid").asInstanceOf[Int]
//    console.log("verify:", id)
//    parse(data)
//      .getOrElse(Json.Null).hcursor
//      .downField("id").downField("Right").get[Int]("value").getOrElse(-2)
//

    // Await.result(myid, 2.seconds)
    2
  }
  val personPresentable: Presentable[Person] =
    Presentable(person => span(s"${person.firstName} ${person.id}"))

  implicit val personEditable: Editable[Person] =
    person =>
      for {
        firstNameEditor <- person.firstName.editor

      } yield
        new Editor[Person] {

          def result: Observable[ValidatedNel[String, Person]] =
            firstNameEditor.result.map(
              _.map {
                var res: String = ""

                x =>
                  val parse_result: Parsed[Formula] = FormulaParser.Parse(x)
                  parse_result match {
                    case Parsed.Success(value, _) =>
                      res = " is " + IndexedFormula
                        .toPrenex(Formula.toIndexed(value)._1)

                    case failure: Parsed.Failure =>
                      res = "Incorrect formula, error:\n" + failure.trace()
                  }

                  Person("Prenex of " + x + res, person.id)

              },
            )

          """def result: Observable[ValidatedNel[String, Person]] =
            firstNameEditor.result.map(
              _.map(x => Person("Prenex of " + x + res)),
            )"""
          def ispnf(for1: String): Observable[ValidatedNel[String, Person]] =
            firstNameEditor.result.map(
              _.map {
                var res: String = ""
                // val client = FetchClientBuilder[IO].create

                x =>
                  val p = FormulaParser.Parse(for1)
                  p match {
                    case Parsed.Success(value, _) =>
                      // val pnf = IndexedFormula
                      // .toPrenex(Formula.toIndexed(value)._1)
                      // val cond = pnf.toString == x
                      // if (cond) res = "Right!"
                      // else res = "Wrong!"
                      // val response = client
                      //  .expect[String](
                      //    s"http://localhost:8090/ispnf?value=$x",
                      //  ).map(resp => res)
                      // val responseText =
                      //  dom.fetch(s"http://localhost:8090/ispnf?value=$x")

                      res = httpGet(
                        Config.baseUrl + "/ispnf?answer=$x&randfor=$for1",
                      )
                      if (res == "Right!") {
                        httpPostScore(
                          50,
                          person.id,
                          Config.baseUrl + "/mod/lti/services.php/2/lineitems/7/lineitem/scores?type_id=1",
                        )

                      }
                      if (res == "Wrong!") {
                        httpPostScore(
                          0,
                          person.id,
                          Config.baseUrl + "/mod/lti/services.php/2/lineitems/7/lineitem/scores?type_id=1",
                        )
                      }

                    // res = responseText
                    // res = responseText
                    case failure: Parsed.Failure =>
                      res = "Incorrect formula, error:\n" + failure.trace()
                  }
                  Person(res, person.id)

              },
            )
          def present: VDomModifier =
            div(
              div("Формула:", firstNameEditor.present),
            )

          override def isnormal2: Observable[ValidatedNel[String, Person]] = ???
        }
}
