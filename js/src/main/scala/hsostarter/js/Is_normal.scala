package hsostarter.js

import cats.effect.SyncIO
import hsostarter.js.Editable.ops._
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import outwatch.{HtmlVNode, VDomModifier, VNode}

import hsostarter.js.Person.getUserId

import scala.concurrent.Future

final class Is_normal(editor: Editor[Person]) {

  // private def req(formul:String):isnorm = {
  // request.withQueryParameter("isnormal",formul).send().map()
  // }
  lazy val formula =
    hsostarter.js.Formula.randomFormula(3, 4, 4).toString

  val nodeforbutton: SyncIO[VNode] = for {
    goHandler <- Handler.create[Boolean]

  } yield
    div(
      cls := "row d-flex flex-column col-9 ml-2 nizbtn",
      button(
        "Получить случайную формулу",
        idAttr := "reductionButton",
        `type` := "submit",
        color := "#000000",
        cls := "btn btn-lg btn-secondary col",
        onClick.use(true) --> goHandler,
      ),
      goHandler.map(if (_) {

        div(formula)
      } else div()),
    )

  val nodeforres: SyncIO[VNode] = for {
    goHandler <- Handler.create[Boolean]

  } yield
    div(
      cls := "row d-flex flex-column col-9 ml-2 nizbtn",
      button(
        "Отправить ответ",
        idAttr := "reductionButton",
        `type` := "submit",
        color := "#000000",
        cls := "btn btn-lg btn-secondary col",
        onClick.use(true) --> goHandler,
      ),
      goHandler.map(if (_) {
        div(
          h1("Результат:"),
          div(
            editor
              .ispnf(formula)
              .map(_.map(person => person.firstName)).map(
                _.getOrElse("∀r.∃f.∀v.R(v)"),
              ),
          ),
        )
      } else div()),
    )

  val node: HtmlVNode =
    div(
      h1("Получите формулу, приведите в ПНФ и введите ваш ответ"),
      nodeforbutton,
      VDomModifier(
        div(
          cls := "niz",
          h1("Ваш ответ:"),
          editor.present,
          nodeforres,
        ),
      ),
    )
}

object Is_normal {
  def init: SyncIO[Is_normal] =
    for {
      editor <- Person(
        "∀r.∃f.∀v.R(v)",
        getUserId,
      ).editor
    } yield new Is_normal(editor)
}
