package hsostarter.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

final case class isnorm(
  value: String,
)

object isnorm {
  implicit val helloEncoder: Encoder[isnorm] = deriveEncoder
  implicit val helloDecoder: Decoder[isnorm] = deriveDecoder
}
