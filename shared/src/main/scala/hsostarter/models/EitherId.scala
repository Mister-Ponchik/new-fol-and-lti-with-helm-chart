package hsostarter.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

final case class EitherId(
  id: Either[String, Int],
)

object EitherId {
  implicit val helloEncoder: Encoder[isnorm] = deriveEncoder
  implicit val helloDecoder: Decoder[isnorm] = deriveDecoder
}
